//
//  DetailsViewModel.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import Foundation

enum FlightDetailsectionTitle: String{
    case ship = "Ships"
    case core = "Cores"
    case none = ""
}

class FlightDetailsViewModel {
    
    var flight: Flight!

    var isBookmarked = false{
        didSet{
            updateBookmarkUI?(isBookmarked)
        }
    }
    
    var isWikipediaEnabled = false{
        didSet{
            updateWikipediaUI?(isWikipediaEnabled)
        }
    }
    
    var databaseRepository: FlightsDetailRepo!
    
    private let staticCellId: [CellId] = [.sliderTableViewCell, .totallDetailTableViewCell, .statusCheckTableViewCell]
    
    var sectionTitles: [FlightDetailsectionTitle] = [.none]
    var tableViewSectionCount: Int{
        return self.sectionTitles.count
    }
    
    var reloadTable: (() -> Void)?
    var updateWikipediaUI: ((Bool) -> Void)?
    var updateBookmarkUI: ((Bool) -> Void)?
    var openURL: ((URL) -> Void)?
    
    
    
    init(flight: Flight, dataRepo: FlightsDetailRepo) {
        self.flight = flight
        self.databaseRepository = dataRepo
    }
    
    
    private func checkBookmarkFromDB(){
        if let id = flight.id{
            isBookmarked = retrieveBookmark(id: id)
        }
    }
    
    private func checkWikipediaLink() {
        isWikipediaEnabled = flight.wikipediaLink != nil
    }
    
    func retrieveBookmark(id: String) -> Bool{
        return self.databaseRepository.retrieveSaved(id: id)
    }
    
    func onViewDidLoad() {
        updateTableViewSectionId()
        checkBookmarkFromDB()
        checkWikipediaLink()
        reloadTable?()
    }
        
    func onBookmarkTapped(){
        isBookmarked.toggle()
        if let id = self.flight.id{
            if isBookmarked{
                self.databaseRepository.saveDoc(id)
            }else{
                self.databaseRepository.removeDoc(id)
            }
        }
    }
    
    func onWikipediaTapped(){
        if let url = URL(string: self.flight.wikipediaLink ?? "") {
            self.openURL?(url)
        }
    }
    
    private func updateTableViewSectionId(){
        ///add ships cell to table
        if flight?.ships?.count ?? 0 > 0{
            sectionTitles.append(.ship)
        }
        ///to add cores cell to table
        if flight?.cores?.count ?? 0 > 0{
            sectionTitles.append(.core)
        }
    }
    
    func tableViewCellCount(section: Int) -> Int{
        if sectionTitles[section] == .ship{
            return self.flight.ships?.count ?? 0
        }else if sectionTitles[section] == .core{
            return self.flight.cores?.count ?? 0
        }else{
            return staticCellId.count
        }
    }
    
    func tableViewCellId(indexPath: IndexPath) -> CellId{
        if sectionTitles[indexPath.section] == .ship{
            return .shipTableViewCell
        }else if sectionTitles[indexPath.section] == .core{
            return .coreTableViewCell
        }else{
            return staticCellId[indexPath.row]
        }
    }
}
