//
//  TitleTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/14/22.
//

import UIKit

class TitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(title: String){
        titleLabel.text = title
    }
    
}
