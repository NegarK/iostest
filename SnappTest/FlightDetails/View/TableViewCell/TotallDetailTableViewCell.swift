//
//  TotallDetailTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/8/22.
//

import UIKit

class TotallDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var flightNumberLabel: UILabel!
    @IBOutlet weak var rocketLabel: UILabel!
    @IBOutlet weak var launchpadLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(flight: Flight){
        nameLabel.text = flight.name
        flightNumberLabel.text = flight.flightNumber
        rocketLabel.text = flight.rocket
        launchpadLabel.text = flight.launchpad
        dateLabel.text =  flight.date
    }
}
