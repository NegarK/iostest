//
//  SliderTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/8/22.
//

import UIKit
import Kingfisher

class SliderTableViewCell: UITableViewCell {

    @IBOutlet weak var sliderImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configure(flight: Flight){
        let url = URL(string: flight.photoPath ?? "")
        sliderImageView.kf.indicatorType = .activity
        sliderImageView.kf.setImage(with: url)
    }
}
