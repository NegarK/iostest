//
//  ShipTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/9/22.
//

import UIKit

class ShipTableViewCell: UITableViewCell {

    @IBOutlet weak var shipLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(ship: String?){
        shipLabel.text = ship
    }
    
}
