//
//  CoreTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/9/22.
//

import UIKit

class CoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coreLabel: UILabel!
    @IBOutlet weak var flightLabel: UILabel!
    @IBOutlet weak var landingTypeLabel: UILabel!
    @IBOutlet weak var landpadLabel: UILabel!
    
    @IBOutlet weak var gridfinsImageView: UIImageView!
    @IBOutlet weak var legsImageView: UIImageView!
    @IBOutlet weak var reusedImageView: UIImageView!
    @IBOutlet weak var landingAttemptImageView: UIImageView!
    @IBOutlet weak var landingSuccessImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(core: Core?){
        
        coreLabel.text = core?.core
        flightLabel.text = "\(core?.flight ?? 0)"
        landingTypeLabel.text = core?.landingType
        landpadLabel.text = core?.landpad
        
        gridfinsImageView.image = UIImage(named: (core?.gridfins == true) ? "checked" : "checkbox")
        legsImageView.image = UIImage(named: (core?.legs == true) ? "checked" : "checkbox")
        reusedImageView.image = UIImage(named: (core?.reused == true) ? "checked" : "checkbox")
        landingAttemptImageView.image = UIImage(named: (core?.landingAttempt == true) ? "checked" : "checkbox")
        landingSuccessImageView.image = UIImage(named: (core?.landingSuccess == true) ? "checked" : "checkbox")
        
        
    }
    
    
}
