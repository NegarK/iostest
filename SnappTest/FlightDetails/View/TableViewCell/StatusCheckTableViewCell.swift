//
//  StatusCheckTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/8/22.
//

import UIKit

class StatusCheckTableViewCell: UITableViewCell {

    @IBOutlet weak var successImageView: UIImageView!
    @IBOutlet weak var netImageView: UIImageView!
    @IBOutlet weak var upcomingImageView: UIImageView!
    @IBOutlet weak var autoUpdateImageView: UIImageView!
    @IBOutlet weak var tbdImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configure(flight: Flight){
        successImageView.image = UIImage(named: (flight.success == true) ? "checked" : "checkbox")
        netImageView.image = UIImage(named: (flight.net == true) ? "checked" : "checkbox")
        upcomingImageView.image = UIImage(named: (flight.upcoming == true) ? "checked" : "checkbox")
        autoUpdateImageView.image = UIImage(named: (flight.autoUpdate == true) ? "checked" : "checkbox")
        tbdImageView.image = UIImage(named: (flight.tbd == true) ? "checked" : "checkbox")
    }
}
