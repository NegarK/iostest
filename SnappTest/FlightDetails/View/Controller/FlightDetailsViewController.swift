//
//  DetailsViewController.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import UIKit

class FlightDetailsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bookmarkButton: UIBarButtonItem!
    @IBOutlet weak var wikipediaButton: UIBarButtonItem!
    
    weak var coordinator: MainCoordinator?
    var viewModel: FlightDetailsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        registerCell()
        setupBinding()
        viewModel.onViewDidLoad()
    }
    
    func reloadTable(){
        viewModel.reloadTable = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.isScrollEnabled = true
                self?.tableView.reloadData()
                self?.tableView.tableFooterView = nil
            }
        }
    }
    
    func setupWikipediaBinding(){
        viewModel.updateWikipediaUI = { [weak self] isEnabled in
            DispatchQueue.main.async {
                if isEnabled{
                    self?.wikipediaButton.isEnabled = true
                }else{
                    self?.wikipediaButton.isEnabled = false
                }
            }
        }
    }
    
    
    func setupBookmarkBinding(){
        viewModel.updateBookmarkUI = { [weak self] isBookmarked in
            DispatchQueue.main.async {
                if isBookmarked{
                    self?.bookmarkButton.image = UIImage(named: "bookmark-fill")
                }else{
                    self?.bookmarkButton.image = UIImage(named: "bookmark")
                }
            }
        }
    }
    
    func setupOpenURLBinding(){
        viewModel.openURL = { url in
            DispatchQueue.main.async {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func setupBinding(){
        reloadTable()
        setupWikipediaBinding()
        setupBookmarkBinding()
        setupOpenURLBinding()
    }
    
    @IBAction func wikipediaButtonAction(_ sender: Any) {
        viewModel.onWikipediaTapped()
    }
    @IBAction func bookmarkButtonAction(_ sender: Any) {
        viewModel.onBookmarkTapped()
    }
    
}

extension FlightDetailsViewController: UITableViewDataSource, UITableViewDelegate{

    func registerCell(){
        self.tableView.register(UINib(nibName: CellId.sliderTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.sliderTableViewCell.rawValue)
        
        self.tableView.register(UINib(nibName: CellId.totallDetailTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.totallDetailTableViewCell.rawValue)
        
        self.tableView.register(UINib(nibName: CellId.statusCheckTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.statusCheckTableViewCell.rawValue)
                        
        self.tableView.register(UINib(nibName: CellId.shipTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.shipTableViewCell.rawValue)

        self.tableView.register(UINib(nibName: CellId.coreTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.coreTableViewCell.rawValue)
        
        self.tableView.register(UINib(nibName: CellId.titleTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.titleTableViewCell.rawValue)

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.tableViewSectionCount
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableViewCellCount(section: section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section != 0{
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellId.titleTableViewCell.rawValue) as? TitleTableViewCell{
                cell.configure(title: viewModel.sectionTitles[section].rawValue)
                return cell
            }
        }
        
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cellId = viewModel.tableViewCellId(indexPath: indexPath)
        
        switch cellId{
        case .sliderTableViewCell:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId.rawValue) as? SliderTableViewCell{
                cell.configure(flight: viewModel.flight)
                return cell
            }
        
        case .totallDetailTableViewCell:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId.rawValue) as? TotallDetailTableViewCell{
                cell.configure(flight: viewModel.flight)
                return cell
            }
        case .statusCheckTableViewCell:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId.rawValue) as? StatusCheckTableViewCell{
                cell.configure(flight: viewModel.flight)
                return cell
            }
            
        case .shipTableViewCell:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId.rawValue) as? ShipTableViewCell{
                cell.configure(ship: (viewModel.flight.ships?[indexPath.row]))
                return cell
            }
        case .coreTableViewCell:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId.rawValue) as? CoreTableViewCell{
                cell.configure(core: (viewModel.flight.cores?[indexPath.row]))
                return cell
            }
            
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
}
