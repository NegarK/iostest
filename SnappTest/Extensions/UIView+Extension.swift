//
//  UIView+Extension.swift
//  SnappTest
//
//  Created by Negar on 9/6/22.
//

import UIKit

class CornerView: UIView{
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}
extension UIView{
    
    func createSpinnerFooter() -> UIView{
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 30))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        spinner.startAnimating()
        spinner.color = .white
        
        footerView.addSubview(spinner)
        return footerView
    }
}
