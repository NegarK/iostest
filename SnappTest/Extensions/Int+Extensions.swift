//
//  Int+Extensions.swift
//  SnappTest
//
//  Created by Negar on 9/9/22.
//

import Foundation

extension Int{
    func convertToDateString() -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        return dateFormatter.string(from: date) + "  " + timeFormatter.string(from: date)
    }
}
