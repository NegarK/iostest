//
//  MainViewModel.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import Foundation

class FlightsViewModel{
    
    var remoteApi: FlightsApi!
    var flights = [Flight](){
        didSet{
            reloadTable?()
        }
    }
    
    var reloadTable: (() -> Void)?
    var showDetail: ((Flight) -> Void)?
    var tableViewCellCount: Int{
        return self.flights.count
    }
    var loadingActivity: ((Bool) -> Void)?
    var handleFooterSpinner: ((Bool) -> Void)?
    var showError: ((String) -> Void)?
        
    var limit = 20
    var pageNumber = 1
    var endOfList = false
    var inProgress = false{
        didSet{
            if inProgress{
                if pageNumber == 1{
                    self.loadingActivity?(true)
                }else{
                    handleFooterSpinner?(true)
                }
            }else{
                self.loadingActivity?(false)
                handleFooterSpinner?(false)
            }
        }
    }
    
    init(api: FlightsApi) {
        self.remoteApi = api
    }
    
    func onViewDidLoad() {
        fetchData()
    }
    
    func createRequestModel() -> RequestDataModel{
        let query = Query(upcoming: false)
        let sort = Sort(flightNumber: "desc")
        let options = Options(limit: self.limit, page: self.pageNumber, sort: sort)
        let encodableModel = RequestDataModel(query: query, options: options)
        
        return encodableModel
    }
    
    func fetchData(){
        guard !inProgress else { return }
        inProgress = true
        
        remoteApi.getFlights(params: createRequestModel()) { [unowned self] (result) in
            self.inProgress = false
            
            switch result{
            case .success(let data):
                self.flights.append(contentsOf: data.docs.map({ $0.asFlight() }))
                            
                if data.hasNextPage == true{
                    self.pageNumber = data.nextPage ?? (self.pageNumber + 1)
                }else{
                    self.endOfList = true
                }
                
            case .failure(let error):

                ///here we can handle error by any error type
                ///according to spaceX API, there is just one error code: 400
                
                switch error{
                case .badRequest:
                    showError?("bad request")
                
                default:
                    showError?("some error occured")
                }
            }
        }
    }
    
    func onFlightCellSelection(index: Int){
        let selectedFlight = flights[index]
        showDetail?(selectedFlight)
    }
    
    func onDisplayCell(index: Int){
        guard !inProgress else{
            return
        }
        
        guard !endOfList else{
            return
        }
        
        ///when the displaying object is in the last 2 objects, we will call the API, we can change it by any other condition
        guard index > flights.count - 3 else{
            return
        }
        
        fetchData()
    }
}
