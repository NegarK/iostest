//
//  FlightDetailUIModel.swift
//  SnappTest
//
//  Created by Negar on 9/10/22.
//

import Foundation
struct Flight {
    var rocketImagePath: String?
    var name: String?
    var flightNumber: String?
    var launchpad: String?
    var rocket: String?
    var date: String?
    var wikipediaLink: String?
    var photoPath: String?
    var success: Bool?
    var upcoming: Bool?
    var autoUpdate: Bool?
    var tbd: Bool?
    var net: Bool?
    var cores: [Core]?
    var ships: [String]?
    var details: String?
    var id: String?
    var description: String?
}
