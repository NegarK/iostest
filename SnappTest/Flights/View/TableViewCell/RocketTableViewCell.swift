//
//  RocketTableViewCell.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import UIKit
import Kingfisher

class RocketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: ShadowGradiant!
    @IBOutlet weak var rocketImageView: UIImageView!
    @IBOutlet weak var flightNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(flight: Flight){
        let url = URL(string: flight.photoPath ?? "")
        rocketImageView.kf.setImage(with: url)
        
        flightNumberLabel.text = flight.flightNumber
        dateLabel.text = flight.date
        statusLabel.text = flight.success == true ? "Success" : "Failure"
        statusLabel.textColor = flight.success == true ? AppColor.greenColor.Color : AppColor.redColor.Color
        
        shadowView.gradiantState = flight.success == true ? .green : .red
        shadowView.layer.masksToBounds = false

        descriptionLabel.text = flight.description
    }
}
