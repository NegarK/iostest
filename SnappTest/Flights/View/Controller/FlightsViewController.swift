//
//  MainViewController.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import UIKit
import Toast_Swift

class FlightsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var tableView: UITableView!
    
    weak var coordinator: MainCoordinator?
    var viewModel: FlightsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Space X"
        registerCell()
        setupBinding()
        viewModel.onViewDidLoad()
    }
}

extension FlightsViewController{
    func setupBinding(){
        handleFooterSpinner()
        reloadTable()
        showError()
        loadingActivity()
        showDetail()
    }
    
    func handleFooterSpinner(){
        viewModel.handleFooterSpinner = { [weak self] hasFooter in
            DispatchQueue.main.async {
                if hasFooter{
                    self?.tableView.tableFooterView = self?.view.createSpinnerFooter()
                }else{
                    self?.tableView.tableFooterView = nil
                }
            }
        }
    }
    
    func reloadTable(){
        viewModel.reloadTable = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.isScrollEnabled = true
                self?.tableView.reloadData()
                self?.tableView.tableFooterView = nil
            }
        }
    }
    
    func showError(){
        viewModel.showError = { [weak self] errorMessage in
            DispatchQueue.main.async {
                self?.view.makeToast(errorMessage, duration: 3.0, position: .top)
            }
        }
    }
    
    func loadingActivity(){
        viewModel.loadingActivity = { [weak self] status in
            DispatchQueue.main.async {
                if status{
                    self?.view.makeToastActivity(.center)
                }else{
                    self?.view.hideToastActivity()
                }
            }
        }
    }
    
    func showDetail() {
        viewModel.showDetail = { [weak self] detail in
            self?.coordinator?.showDetail(flight: detail)
        }
    }
}


extension FlightsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func registerCell(){
        self.tableView.register(UINib(nibName: CellId.rocketTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.rocketTableViewCell.rawValue)
        self.tableView.register(UINib(nibName: CellId.emptyTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellId.emptyTableViewCell.rawValue)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.tableViewCellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellId.rocketTableViewCell.rawValue) as? RocketTableViewCell{
            cell.configure(flight: viewModel.flights[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.onFlightCellSelection(index: indexPath.row)
    }
    
    ///if list is empty we can handle it by table footer view
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.viewModel.inProgress{
            return UIView()
        }else{
            if self.viewModel.tableViewCellCount == 0{
                if let cell = tableView.dequeueReusableCell(withIdentifier: CellId.emptyTableViewCell.rawValue) as? EmptyTableViewCell{
                    return cell
                }else{
                    return UIView()
                }
            }else{
                return UIView()
            }
        }
        
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.viewModel.inProgress{
            return 0.0
        }else{
            if self.viewModel.tableViewCellCount == 0{
                return self.tableView.frame.height
            }
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.onDisplayCell(index: indexPath.row)
    }
    
}
