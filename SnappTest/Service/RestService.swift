//
//  RestService.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import Foundation

typealias dictionary = [String: Any]

enum RequestError: Error {
    
    case badRequest
    case badJson
    case serverError
    case timeOut
    case cancellRequest
    case emptyData
}

enum Method: String{
    case post = "POST"
    case get = "GET"
}

enum API: String{
    case launchQuery = "v5/launches/query/"
     
    var path: String{
        return AppConfig.baseURL.rawValue + self.rawValue
    }
    var method: String {
        
        switch self{
        case .launchQuery:
            return Method.post.rawValue
        }
    }
}

class RestService{
    fileprivate var session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func sendRequest<T: Encodable, U: Decodable>(urlAPI: API, decodeModel: U.Type, parameters: T, completionHandler: @escaping (Result<U, RequestError>) -> Void){
        
        let url = URL(string: urlAPI.path)
        var request = URLRequest(url: url!)
        request.httpMethod = urlAPI.method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        let jsonData = try? JSONEncoder().encode(parameters)
        request.httpBody = jsonData
        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let httpError = error{
                if httpError.localizedDescription != "cancelled"{
                    if error?._code == -1001 {
                        completionHandler(.failure(.timeOut))
                    }else{
                        completionHandler(.failure(httpError as? RequestError ?? .serverError))
                    }
                }else{
                    completionHandler(.failure(.cancellRequest))
                }
            }else{
                if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                    case 200:
                        guard let responseData = data else {
                            completionHandler(.failure(.emptyData))
                            return
                        }
                        do {
                            let apiResponse = try JSONDecoder().decode(decodeModel, from: responseData)
                            completionHandler(.success(apiResponse))
                        }catch{
                            completionHandler(.failure(.badJson))
                        }
                        
                    case 400:
                        completionHandler(.failure(.badRequest))
                        
                    default:
                        completionHandler(.failure(.serverError))
                    }
                }
            }
        }
        task.resume()
    }
}
