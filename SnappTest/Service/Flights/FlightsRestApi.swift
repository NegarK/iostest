//
//  FlightsRestApi.swift
//  SnappTest
//
//  Created by Negar on 9/10/22.
//

import Foundation

struct FlightsRestApi: FlightsApi {
    let remoteApi: RestService!
    
    init(remoteApi: RestService) {
        self.remoteApi = remoteApi
    }
    
    func getFlights(params: RequestDataModel,
                    completion: @escaping (Result<ResultDataModel,RequestError>) -> Void) {
        self.remoteApi.sendRequest(urlAPI: API.launchQuery, decodeModel: ResultDataModel.self, parameters: params) { result in
            
            switch result{
            case .success(let data):
                completion(.success(data))
                
            case .failure(let error):                
                completion(.failure(error))
            }
        }
    }
}
