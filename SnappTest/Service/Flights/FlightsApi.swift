//
//  FlightsApi.swift
//  SnappTest
//
//  Created by Negar on 9/10/22.
//

import Foundation

protocol FlightsApi {
    func getFlights(params: RequestDataModel,
                    completion: @escaping (Result<ResultDataModel,RequestError>) -> Void)
}
