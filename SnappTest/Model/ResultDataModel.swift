//
//  ResultDataModel.swift
//  SnappTest
//
//  Created by Negar on 9/6/22.
//

import Foundation

// MARK: - ResultDataModel
struct ResultDataModel: Codable {
    let docs: [Doc]
    let totalDocs, offset, limit, totalPages: Int?
    let page, pagingCounter: Int?
    let hasPrevPage, hasNextPage: Bool?
    let prevPage, nextPage: Int?
    
    enum CodingKeys: String, CodingKey {
        case docs = "docs"
        case totalDocs = "totalDocs"
        case offset = "offset"
        case limit = "limit"
        case totalPages = "totalPages"
        case page = "page"
        case pagingCounter = "pagingCounter"
        case hasPrevPage = "hasPrevPage"
        case hasNextPage = "hasNextPage"
        case prevPage = "prevPage"
        case nextPage = "nextPage"
    }
}

struct Doc: Codable {
    let fairings: Fairings?
    let links: Links?
    let staticFireDateUTC: String?
    let staticFireDateUnix: Int?
    let net: Bool?
    let window: Int?
    let rocket: String?
    let success: Bool?
    let failures: [Failure]?
    let details: String?
    let ships, capsules, payloads: [String]?
    let crew: [Crew]?
    let launchpad: String?
    let flightNumber: Int?
    let name, dateUTC: String?
    let dateUnix: Int?
    let dateLocal: String?
    let datePrecision: String?
    let upcoming: Bool?
    let cores: [Core]?
    let autoUpdate, tbd: Bool?
    let launchLibraryID, id: String?

    enum CodingKeys: String, CodingKey {
        case fairings = "fairings"
        case links = "links"
        case staticFireDateUTC = "static_fire_date_utc"
        case staticFireDateUnix = "static_fire_date_unix"
        case net = "net"
        case window = "window"
        case rocket = "rocket"
        case success = "success"
        case failures = "failures"
        case details = "details"
        case crew = "crew"
        case ships = "ships"
        case capsules = "capsules"
        case payloads = "payloads"
        case launchpad = "launchpad"
        case flightNumber = "flight_number"
        case name = "name"
        case dateUTC = "date_utc"
        case dateUnix = "date_unix"
        case dateLocal = "date_local"
        case datePrecision = "date_precision"
        case upcoming = "upcoming"
        case cores = "cores"
        case autoUpdate = "auto_update"
        case tbd = "tbd"
        case launchLibraryID = "launch_library_id"
        case id = "id"
    }
    
    func asFlight() -> Flight {
        let flight = Flight(rocketImagePath: self.links?.patch?.small,
                            name: self.name,
                            flightNumber: "\(self.flightNumber ?? 0)",
                            launchpad: self.launchpad,
                            rocket: self.rocket,
                            date: (self.dateUnix ?? 0).convertToDateString(),
                            wikipediaLink: self.links?.wikipedia,
                            photoPath: self.links?.patch?.small,
                            success: self.success,
                            upcoming: self.upcoming,
                            autoUpdate: self.autoUpdate,
                            tbd: self.tbd,
                            net: self.net,
                            cores: self.cores,
                            ships: self.ships,
                            details: self.details,
                            id: self.id,
                            description: self.details)
        
        return flight
    }
}

struct Core: Codable {
    let core: String?
    let flight: Int?
    let gridfins, legs, reused, landingAttempt, landingSuccess: Bool?
    let landingType, landpad: String?

    enum CodingKeys: String, CodingKey {
        case core = "core"
        case flight = "flight"
        case gridfins = "gridfins"
        case legs = "legs"
        case reused = "reused"
        case landingAttempt = "landing_attempt"
        case landingSuccess = "landing_success"
        case landingType = "landing_type"
        case landpad = "landpad"
    }
    
}

struct Fairings: Codable {
    let reused, recoveryAttempt, recovered: Bool?
    let ships: [String]?

    enum CodingKeys: String, CodingKey {
        case reused = "reused"
        case recovered = "recovered"
        case recoveryAttempt = "recovery_attempt"
        case ships = "ships"
    }
}

struct Links: Codable {
    let patch: Patch?
    let reddit: Reddit?
    let flickr: Flickr?
    let presskit, webcast, youtubeID, article, wikipedia: String?

    enum CodingKeys: String, CodingKey {
        case patch = "patch"
        case reddit = "reddit"
        case flickr = "flickr"
        case presskit = "presskit"
        case webcast = "webcast"
        case youtubeID = "youtube_id"
        case article = "article"
        case wikipedia = "wikipedia"
    }
}

struct Flickr: Codable {
    let small, original: [String]?
    
    enum CodingKeys: String, CodingKey {
        case small = "small"
        case original = "original"
    }
}

struct Patch: Codable {
    let small, large: String?
    
    enum CodingKeys: String, CodingKey {
        case small = "small"
        case large = "large"
    }
}

struct Reddit: Codable {
    let campaign, launch, media, recovery: String?
    
    enum CodingKeys: String, CodingKey {
        case campaign = "campaign"
        case launch = "launch"
        case media = "media"
        case recovery = "recovery"
    }
}

struct Crew: Codable {
    let crew, role: String?
    
    enum CodingKeys: String, CodingKey {
        case crew = "crew"
        case role = "role"
    }
}

struct Failure: Codable {
    let time: Int?
    let altitude: Int?
    let reason: String?
    
    enum CodingKeys: String, CodingKey {
        case time = "time"
        case altitude = "altitude"
        case reason = "reason"
    }
}
