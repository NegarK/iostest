//
//  RequestDataModel.swift
//  SnappTest
//
//  Created by Negar on 9/6/22.
//

import Foundation

//@propertyWrapper
//struct NullEncodable<T>: Encodable where T: Encodable {
//
//    var wrappedValue: T?
//
//    init(wrappedValue: T?) {
//        self.wrappedValue = wrappedValue
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch wrappedValue {
//        case .some(let value): try container.encode(value)
//        case .none: try container.encodeNil()
//        }
//    }
//}

// MARK: - RequestDataModel
struct RequestDataModel: Encodable {
    var query: Query
    var options: Options
    
    enum CodingKeys: String, CodingKey {
        case query = "query"
        case options = "options"
    }
}
struct Query: Codable {
    let upcoming: Bool
    
    enum CodingKeys: String, CodingKey {
        case upcoming = "upcoming"
    }
}

struct Options: Codable {
    let limit: Int
    let page: Int
    let sort: Sort
    
    enum CodingKeys: String, CodingKey {
        case limit = "limit"
        case page = "page"
        case sort = "sort"
    }
}
struct Sort: Codable {
    let flightNumber: String
    
    enum CodingKeys: String, CodingKey {
        case flightNumber = "flight_number"
    }
}
enum SortType: String{
    case desc = "asc"
    case asc = "desc"
}
