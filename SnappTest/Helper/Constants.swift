//
//  Constants.swift
//  SnappTest
//
//  Created by Negar on 9/5/22.
//

import Foundation

enum AppConfig: String{
    case baseURL = "https://api.spacexdata.com/"
}

enum CellId: String{
    case rocketTableViewCell = "RocketTableViewCell"
    case emptyTableViewCell = "EmptyTableViewCell"
    case sliderTableViewCell = "SliderTableViewCell"
    case totallDetailTableViewCell = "TotallDetailTableViewCell"
    case statusCheckTableViewCell = "StatusCheckTableViewCell"
    case shipTableViewCell = "ShipTableViewCell"
    case shipsTableViewCell = "ShipsTableViewCell"
    case coreTableViewCell = "CoreTableViewCell"
    case coresTableViewCell = "CoresTableViewCell"
    case titleTableViewCell = "TitleTableViewCell"
}

enum StoryboardName: String{
    case main = "Main"
    case flights = "Flights"
    case flightsDetail = "FlightDetail"
}
