//
//  ShadowGradient.swift
//  SnappTest
//
//  Created by Negar on 9/7/22.
//

import UIKit

enum GradiantState: String {
    case clear = "clear"
    case green = "green"
    case red   = "red"
}

class ShadowGradiant: CornerView{
    
    enum GradiatnColors: String {

        case greenFirst      = "#02F9AE00"
        case greenSecond     = "#58F6B314"
        case greenThird      = "#72F5B43D"

        case redFirst        = "#EE7D8D00"
        case redSecond       = "#EE7D8D14"
        case redThird        = "#EE7D8D3D"

        case clear            = "#000000"
        
    }
    
    let clearColor = [UIColor(hex: GradiatnColors.clear.rawValue).cgColor]
    
    let greenColors = [UIColor(hex: GradiatnColors.greenFirst.rawValue).cgColor, UIColor(hex: GradiatnColors.greenSecond.rawValue).cgColor, UIColor(hex: GradiatnColors.greenThird.rawValue).cgColor]
    
    let redColors = [UIColor(hex: GradiatnColors.redFirst.rawValue).cgColor, UIColor(hex: GradiatnColors.redSecond.rawValue).cgColor, UIColor(hex: GradiatnColors.redThird.rawValue).cgColor]
    
    @IBInspectable var startLocation: Double = 0.0 { didSet { updateLocations() }}
    @IBInspectable var endLocation: Double = 1.0 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode: Bool = false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode: Bool = false { didSet { updatePoints() }}
    @IBInspectable var state: String = "clear" {
        didSet {
            gradiantState = GradiantState.init(rawValue: state)  ?? .clear
            updateColors()
        }
    }
    
    var colors: [UIColor] = []
    override public class var layerClass: AnyClass { CAGradientLayer.self }
    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }
    var gradiantState: GradiantState = .clear{
        didSet{
            updateColors()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        updateColors()
        updatePoints()
    }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.0, y: 0.0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.0, y: 1.0)
        }
    }
    
    func updateColors() {
        switch gradiantState {
        case .green:
            gradientLayer.colors = !horizontalMode ? greenColors : greenColors.reversed()
         
        case .red:
            gradientLayer.colors = !horizontalMode ? redColors : redColors.reversed()
           
        case .clear:
            gradientLayer.colors = clearColor
            
        }
    }
    
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
}

