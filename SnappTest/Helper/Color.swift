//
//  Color.swift
//  SnappTest
//
//  Created by Negar on 9/10/22.
//

import UIKit

enum AppColor: String {
    case redColor = "RedColor"
    case blackColor = "BlackColor"
    case greenColor = "GreenColor"
    case grayColor = "GrayColor"
    
    var Color: UIColor{
        return  UIColor(named: self.rawValue)!
    }
}
