//
//  CoreDataFlightsDetailRepo.swift
//  SnappTest
//
//  Created by Negar on 9/10/22.
//

import UIKit
import CoreData

struct CoreDataFlightsDetailRepo: FlightsDetailRepo {
    
    func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    func saveDoc(_ id: String){
        guard let context = getContext() else { return }
        
        let newBookmark = NSEntityDescription.insertNewObject(forEntityName: "Bookmark", into: context)
        newBookmark.setValue(id, forKey: "id")
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Failed to save session data! \(error): \(error.userInfo)")
        }
    }
    func removeDoc(_ id: String) {
        guard let context = getContext() else { return }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bookmark")
        
        if let results = try? (context.fetch(fetchRequest) as! [NSManagedObject]){
            for result in results{
                if let savedId = result.value(forKey: "id") as? String, savedId == id{
                    context.delete(result)
                }
            }
        }
        do {
            try context.save()
        } catch let error as NSError {
            print("Failed to remove session data! \(error): \(error.userInfo)")
        }
    }
    
    func retrieveSaved(id: String) -> Bool {
        guard let context = getContext() else { return false }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bookmark")
        
        if let results = try? (context.fetch(fetchRequest) as! [NSManagedObject]){
            for result in results{
                if let savedId = result.value(forKey: "id") as? String, savedId == id{
                    return true
                }
            }
        }
        
        return false
    }
    
}
