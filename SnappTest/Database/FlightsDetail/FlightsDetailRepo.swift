//
//  FlightsDetailRepo.swift
//  SnappTest
//
//  Created by Negar on 9/10/22.
//

import Foundation

protocol FlightsDetailRepo {
    func saveDoc(_ id: String)
    func removeDoc(_ id: String)
    func retrieveSaved(id: String) -> Bool
}
