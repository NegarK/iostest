//
//  MainCoordinator.swift
//  CoordinateApp
//
//  Created by Negar on 8/2/22.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = FlightsViewController.instantiate(storyboardName: StoryboardName.flights.rawValue)
        vc.coordinator = self
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 30.0
        let session = URLSession(configuration: sessionConfig)
        
        let rest = RestService(session: session)
        let flightsApi = FlightsRestApi(remoteApi: rest)
        vc.viewModel = FlightsViewModel(api: flightsApi)
        navigationController.viewControllers = [vc]
    }
    
    func showDetail(flight: Flight) {
        let vc = FlightDetailsViewController.instantiate(storyboardName: StoryboardName.flightsDetail.rawValue)
        vc.coordinator = self
        let detailDataRepo = CoreDataFlightsDetailRepo()
        vc.viewModel = FlightDetailsViewModel(flight: flight,
                                              dataRepo: detailDataRepo)
        navigationController.pushViewController(vc, animated: true)
    }
}
